<?php

namespace Drupal\xray_audit_insight\Plugin\insights;

use Drupal\xray_audit_insight\Plugin\XrayAuditInsightPluginBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\xray_audit\Form\SettingsForm;
use Drupal\xray_audit\Plugin\XrayAuditTaskPluginInterface;

/**
 * Plugin implementation for nodes with excessive revisions.
 *
 * @XrayAuditInsightPlugin (
 *   id = "nodes_with_excessive_revisions",
 *   label = @Translation("Nodes with Excessive Revisions"),
 *   description = @Translation("Identify nodes that exceed the maximum number of revisions allowed"),
 *   sort = 4
 * )
 */
class XrayAuditNodeRevision extends XrayAuditInsightPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getInsightsForDrupalReport(): array {
    $insights = $this->getInsights();
    $excessive_nodes = $insights['nodes_with_excessive_revisions'];
    $url = Url::fromUserInput($this->getPathReport('queries_data_nodes', 'number_nodes_by_revisions'));
    $reports_link = Link::fromTextAndUrl($this->t('Reports'), $url);

    if (empty($excessive_nodes)) {
      $value = $this->t("No nodes exceed the revision limit.");
      $severity = NULL;
    }
    else {
      $value = $this->t('There are nodes that exceed the revision limit. Please check the @report_link for more details.', [
        '@report_link' => $reports_link->toString(),
      ]);
      $severity = REQUIREMENT_WARNING;
    }

    return [
      'nodes_with_excessive_revisions' => $this->buildInsightForDrupalReport(
        $this->label(),
        $value,
        '',
        $severity
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getInsights(): array {
    return ['nodes_with_excessive_revisions' => $this->getNodesWithExcessiveRevisions()];
  }

  /**
   * Get nodes with excessive revisions.
   *
   * @return array
   *   List of node data with excessive revisions.
   */
  protected function getNodesWithExcessiveRevisions(): array {
    $max_revision_count = $this->configFactory->get(SettingsForm::SETTINGS)->get('revisions_thresholds.node');

    $nodes = [];
    if (!empty($max_revision_count)) {
      $operation = 'number_nodes_by_revisions';
      $task_plugin_id = 'queries_data_nodes';
      $task_plugin = $this->getInstancedPlugin($task_plugin_id, $operation);
      $result = $task_plugin instanceof XrayAuditTaskPluginInterface ? $task_plugin->getDataOperationResult($operation) : [];
      $data = $result['results_table'] ?? [];
      $nodes = array_filter($data, fn($item) => $item['revisions']['data'] > $max_revision_count);

      array_multisort(
        array_column($nodes, 'label'),
        $nodes
      );
    }

    return $nodes;
  }

  /**
   * {@inheritdoc}
   */
  public function buildInsightForSettings(): array {
    $build = parent::buildInsightForSettings();
    return $build;
  }

}
