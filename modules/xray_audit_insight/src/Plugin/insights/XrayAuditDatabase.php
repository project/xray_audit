<?php

namespace Drupal\xray_audit_insight\Plugin\insights;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\xray_audit_insight\Plugin\XrayAuditInsightPluginBase;
use Drupal\xray_audit\Plugin\XrayAuditTaskPluginInterface;
use Drupal\xray_audit\Form\SettingsForm;

/**
 * Plugin implementation for nodes with excessive revisions.
 *
 * @XrayAuditInsightPlugin (
 *   id = "suspicious_database_table_size",
 *   label = @Translation("Suspicious database table size."),
 *   description = @Translation("Identified database tables with excessive or suspicious sizes"),
 *   sort = 6
 * )
 */
class XrayAuditDatabase extends XrayAuditInsightPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getInsightsForDrupalReport(): array {
    $insights = $this->getInsights();
    $excessive_size = $insights['suspicious_database_table_size'];
    if ($excessive_size) {
      $url = Url::fromUserInput($this->getPathReport('database_general', 'table_size'));
      $reports_link = Link::fromTextAndUrl($this->t('Reports'), $url);
      $value = $this->t('There are tables that exceed the revision limit. Please check the @report_link for more details', [
        '@report_link' => $reports_link->toString(),
      ]);
      $severity = REQUIREMENT_WARNING;
    }
    else {
      $value = $this->t("No tables exceed the revision limit.");
      $severity = NULL;
    }

    return [
      'suspicious_database_table_size' => $this->buildInsightForDrupalReport(
        $this->label(),
        $value,
        '',
        $severity
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getInsights(): array {
    return ['suspicious_database_table_size' => $this->existTablesWithExcessiveSize()];
  }

  /**
   * Check if there are tables with excessive size.
   *
   * @return bool
   *   True if there are cases  .
   */
  protected function existTablesWithExcessiveSize(): bool {
    $max_size_tables = $this->configFactory->get(SettingsForm::SETTINGS)->get('size_thresholds.tables');
    if (!empty($max_size_tables)) {
      $operation = 'summary';
      $task_plugin_id = 'database_general';
      $task_plugin = $this->getInstancedPlugin($task_plugin_id, $operation);
      $result = $task_plugin instanceof XrayAuditTaskPluginInterface ? $task_plugin->getDataOperationResult($operation) : [];
      $data = $result['largest_tables'] ?? [];
      foreach ($data as $table) {
        if ($table['total_size']['data'] > $max_size_tables) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildInsightForSettings(): array {
    $build = parent::buildInsightForSettings();
    return $build;
  }

}
