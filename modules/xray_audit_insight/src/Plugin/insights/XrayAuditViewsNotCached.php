<?php

namespace Drupal\xray_audit_insight\Plugin\insights;

use Drupal\xray_audit\Plugin\XrayAuditTaskPluginInterface;
use Drupal\xray_audit_insight\Plugin\XrayAuditInsightPluginBase;

/**
 * Plugin implementation of queries_data_node.
 *
 * @XrayAuditInsightPlugin (
 *   id = "views",
 *   label = @Translation("Views not cached"),
 *   description = @Translation("Views not cached"),
 *   sort = 3
 * )
 */
class XrayAuditViewsNotCached extends XrayAuditInsightPluginBase {

  /**
   * List of contrib and core modules with views not cached.
   */
  const EXCLUDED_VIEWS = [
    'redirect_404',
    'watchdog',
    'watchdog_statistics',
  ];

  /**
   * {@inheritdoc}
   */
  public function getInsights(): array {
    return [
      'views_not_cached' => $this->isSomeViewsNotCached(),
      'views_access_anonymous' => $this->isAdmViewsAccessibleAnonymous(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getInsightsForDrupalReport(): array {
    $insights = $this->getInsights();
    $this->buildInsightsCacheViews($insights);
    $this->buildAdmViewsAccessibleAnonymous($insights);
    return $insights;
  }

  /**
   * Check if there are views that are not cached.
   *
   * @return array
   *   Views not cached.
   */
  protected function isSomeViewsNotCached(): array {
    $result = [];
    $data = $this->getViewsData();
    if (empty($data)) {
      return $result;
    }
    $config_excluded = $this->getConfigExcludedViews();
    foreach ($data['results_table'] as $view) {
      if (!in_array($view['id_view'], self::EXCLUDED_VIEWS)
        && !in_array($view['id_view'], $config_excluded)
        && $view['cache_max_age'] === 'No cache') {
        $result[] = $view['id_view'] . ':' . $view['id_display'];
      }
    }

    return $result;
  }

  /**
   * Build insights for views cache.
   *
   * @param array $insights
   *   Insights.
   */
  protected function buildInsightsCacheViews(array &$insights) {
    $title = $this->t('Views cache');
    $value = '';
    $severity = NULL;

    if (empty($insights['views_not_cached'])) {
      $description = $this->t("Cache is enabled for all views");
    }
    else {
      $value = $this->t(
        'There are views that are not cached, which can generate pages with poor cacheability (<a href="@views_report">Views Report</a>):',
        ['@views_report' => $this->getPathReport('views', 'views')]
      );
      $description = $this->t(
        '@views.',
        ['@views' => implode(', ', $insights['views_not_cached'])]
      );
      $severity = REQUIREMENT_WARNING;
    }

    $insights['views_not_cached'] =
      $this->buildInsightForDrupalReport(
        $title,
        $value,
        $description,
        $severity);
  }

  /**
   * Get excluded views from report.
   *
   * @return array
   *   Views list.
   */
  public function getConfigExcludedViews(): array {
    $config_excluded = $this->configFactory->get('xray_audit_insight.settings')->get('excluded_views') ?? [];
    return $config_excluded;
  }

  /**
   * Build insights for views cache.
   *
   * @return array
   *   Results.
   */
  protected function isAdmViewsAccessibleAnonymous(): array {
    $result = [];
    $data = $this->getViewsData();
    if (empty($data)) {
      return $result;
    }
    foreach ($data['results_table'] as $view) {
      if ($view['warning_anonymous_user_can_access']) {
        $result[] = $view['id_view'] . ':' . $view['id_display'];
      }
    }
    return $result;
  }

  /**
   * Build insights for admin views accessible by anonymous.
   *
   * @param array $insights
   *   Insights.
   */
  protected function buildAdmViewsAccessibleAnonymous(array &$insights) {
    $title = $this->t('Admin views accessible by anonymous');
    $value = '';
    $severity = NULL;

    if (empty($insights['views_access_anonymous'])) {
      $description = $this->t("There are no admin views accessible by anonymous users.");
    }
    else {
      $value = $this->t(
        'There are admin views accessible to anonymous users, which may pose a security risk. (<a href="@views_report">Views Report</a>):',
        ['@views_report' => $this->getPathReport('views', 'views')]
      );
      $description = $this->t(
        '@views.',
        ['@views' => implode(', ', $insights['views_access_anonymous'])]
      );
      $severity = REQUIREMENT_WARNING;
    }
    $insights['views_access_anonymous'] = $this->buildInsightForDrupalReport(
        $title,
        $value,
        $description,
        $severity);
  }

  /**
   * {@inheritdoc}
   */
  public function buildInsightForSettings(): array {
    $build = parent::buildInsightForSettings();
    $options = [];
    $data = $this->getViewsData();
    if (empty($data)) {
      return $build;
    }
    foreach ($data['results_table'] as $view) {
      if (isset($view['id_view']) && !in_array($view['id_view'], self::EXCLUDED_VIEWS)) {
        $options[$view['id_view']] = $view['id_view'];
      }
    }

    $default = $this->getConfigExcludedViews();
    $build['excluded_views'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Views to exclude'),
      '#options' => $options,
      '#default_value' => $default,
      '#config_target' => 'xray_audit_insight.settings:excluded_views',
      '#description' => $this->t('Choose the methods that will be available for the user when cancelling the account'),
    ];
    return $build;

  }

  /**
   * Get Views data.
   *
   * @return array
   *   Views data.
   */
  protected function getViewsData(): array {
    static $data;

    if (!isset($data)) {
      $task_plugin = $this->getInstancedPlugin('views', 'views');
      if (!$task_plugin instanceof XrayAuditTaskPluginInterface) {
        return [];
      }
      $data = $task_plugin->getDataOperationResult('views');
    }
    return $data;
  }

}
