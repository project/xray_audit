<?php

namespace Drupal\xray_audit_insight\Plugin\insights;

use Drupal\xray_audit_insight\Plugin\XrayAuditInsightPluginBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\xray_audit\Form\SettingsForm;
use Drupal\xray_audit\Plugin\XrayAuditTaskPluginInterface;

/**
 * Plugin implementation for paragraphs with excessive revisions.
 *
 * @XrayAuditInsightPlugin (
 *   id = "paragraphs_with_excessive_revisions",
 *   label = @Translation("Paragraphs with Excessive Revisions"),
 *   description = @Translation("Identify paragraphs that exceed the maximum number of revisions allowed"),
 *   sort = 4
 * )
 */
class XrayAuditParagraphRevision extends XrayAuditInsightPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getInsightsForDrupalReport(): array {
    $insights = $this->getInsights();
    $excessive_paragraphs = $insights['paragraphs_with_excessive_revisions'];
    $url = Url::fromUserInput($this->getPathReport('queries_data_paragraphs', 'number_paragraphs_by_revisions'));
    $reports_link = Link::fromTextAndUrl($this->t('Reports'), $url);

    if (empty($excessive_paragraphs)) {
      $value = $this->t("No paragraphs exceed the revision limit.");
      $severity = NULL;
    }
    else {
      $value = $this->t('There are paragraphs that exceed the revision limit. Please check the @report_link for more details.', [
        '@report_link' => $reports_link->toString(),
      ]);
      $severity = REQUIREMENT_WARNING;
    }

    return [
      'paragraphs_with_excessive_revisions' => $this->buildInsightForDrupalReport(
        $this->label(),
        $value,
        '',
        $severity
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getInsights(): array {
    return ['paragraphs_with_excessive_revisions' => $this->getParagraphsWithExcessiveRevisions()];
  }

  /**
   * Get paragraphs with excessive revisions.
   *
   * @return array
   *   List of paragraph data with excessive revisions.
   */
  protected function getParagraphsWithExcessiveRevisions(): array {
    $max_revision_count = $this->configFactory->get(SettingsForm::SETTINGS)->get('revisions_thresholds.paragraph');
    $paragraphs = [];

    if (!empty($max_revision_count)) {
      $operation = 'number_paragraphs_by_revisions';
      $task_plugin_id = 'queries_data_paragraphs';
      $task_plugin = $this->getInstancedPlugin($task_plugin_id, $operation);
      $result = $task_plugin instanceof XrayAuditTaskPluginInterface ? $task_plugin->getDataOperationResult($operation) : [];
      $data = $result['results_table'] ?? [];
      $paragraphs = array_filter($data, fn($item) => $item['revisions']['data'] > $max_revision_count);

      array_multisort(
        array_column($paragraphs, 'label'),
        $paragraphs
      );
    }

    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function buildInsightForSettings(): array {
    $build = parent::buildInsightForSettings();
    return $build;
  }

}
