<?php

namespace Drupal\xray_audit;

use Drupal\xray_audit\Services\CsvDownloadManagerInterface;

/**
 * Provides a trait for csv generation and download in audit tasks.
 *
 * This trait is meant to make it easier to implement csv download
 * functionality. Make sure to called handleCsv and implement abstract methods.
 */
trait XrayAuditTaskCsvDownloadTrait {

  /**
   * Csv Download Service.
   *
   * @var \Drupal\xray_audit\Services\CsvDownloadManagerInterface
   */
  protected $csvDownloadService;

  /**
   * Prepare csv data.
   *
   * @return array
   *   Prepared csv data.
   */
  abstract protected function prepareCsvHeaders(string $operation): array;

  /**
   * Prepare csv data.
   *
   * @param string $operation
   *   Operation.
   * @param array $data
   *   Optional base data to use for preparing the csv data.
   *
   * @return array
   *   Prepared csv data.
   */
  abstract protected function prepareCsvData(string $operation, array ...$data): array;

  /**
   * Handle csv generation and downloading.
   *
   * @param string $operation
   *   Operation.
   * @param array $data
   *   Optional base data to pass to csv prepare data method.
   */
  protected function handleCsv(string $operation, array ...$data): void {
    $csv_download_service = $this->getCsvDownloadService();

    if (!$csv_download_service->downloadCsv()) {
      return;
    }

    $csv_headers = $this->prepareCsvHeaders($operation);
    $csv_data = $this->prepareCsvData($operation, ...$data);
    $csv_download_service->createCsv($csv_data, $csv_headers, $operation);
  }

  /**
   * Create renderable link.
   *
   * @param string $operation
   *   Operation.
   * @param array $extra_options
   *   Extra options that will be merged with the link url current set options.
   *
   * @return array
   *   Link render array.
   */
  protected function createRenderableLink(string $operation, string $title = '', array $extra_options = []): array {
    $csv_download_service = $this->getCsvDownloadService();
    $link = $csv_download_service->createLink($operation);
    $url = $link->getUrl();

    $default_options = [
      'attributes' => [
        'class' => [
          'button',
          'button--primary',
          'button--small',
        ],
      ],
    ];

    $url->mergeOptions($default_options);

    if (!empty($extra_options)) {
      $url->mergeOptions($extra_options);
    }

    if (!empty($title)) {
      $link->setText($title);
    }

    return $link->toRenderable();
  }

  /**
   * Get csv Download service.
   *
   * @return \Drupal\xray_audit\Services\CsvDownloadManagerInterface
   *   Csv Download service.
   */
  protected function getCsvDownloadService(): CsvDownloadManagerInterface {
    if (empty($this->csvDownloadService)) {
      $this->csvDownloadService = \Drupal::service('xray_audit.csv_download_manager');
    }

    return $this->csvDownloadService;
  }

}
