<?php

namespace Drupal\xray_audit\Plugin\xray_audit\groups;

use Drupal\xray_audit\Plugin\XrayAuditGroupPluginBase;

/**
 * Plugin implementation of the xray_audit_group_plugin.
 *
 * @XrayAuditGroupPlugin(
 *   id = "layout",
 *   label = @Translation("Layout"),
 *   description = @Translation("Layout."),
 *   sort = 6
 * )
 */
class LayoutGroupPlugin extends XrayAuditGroupPluginBase {

}
