<?php

namespace Drupal\xray_audit\Plugin\xray_audit\tasks\Layout;

use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Link;
use Drupal\xray_audit\Plugin\xray_audit\tasks\ContentMetric\XrayAuditQueryTaskPluginBase;
use Drupal\xray_audit\XrayAuditTaskCsvDownloadTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @todo dependency injection.
 *
 * Plugin implementation of Block layout.
 *
 * @XrayAuditTaskPlugin (
 *   id = "block_layout",
 *   label = @Translation("Block layout"),
 *   description = @Translation("Block layout."),
 *   group = "layout",
 *   sort = 1,
 *   operations = {
 *      "blocks_configurations" = {
 *          "label" = "Blocks configurations",
 *      }
 *   },
 *   dependencies = {"block"}
 * )
 */
class XrayAuditBlockLayoutPlugin extends XrayAuditQueryTaskPluginBase {

  use XrayAuditTaskCsvDownloadTrait;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * Formats visibility conditions into a list render array.
   *
   * @param \Drupal\Core\Condition\ConditionPluginCollection $conditions
   *   A collection of visibility conditions from the block config.
   *
   * @return array
   *   A list render array of visibility conditions.
   */
  protected function formatVisibilityConditions(ConditionPluginCollection $conditions): array {
    $list = [
      '#theme' => 'item_list',
      '#items' => [],
      '#attributes' => ['class' => ['visibility-conditions-list']],
    ];

    foreach ($conditions as $condition_id => $condition) {
      // Catch every exception that can be thrown by the condition plugin.
      // This way we can still display the other conditions.
      // And we prevent page from not being displayed.
      // A bad implementation in the summary method, for example using invalid
      // contexts, can cause a fatal error.
      try {
        $summary = (string) $condition->summary();
      }
      catch (\Exception $e) {
        $summary = $this->t('Unknown conditions');
      }
      finally {
        $plugin_definition = $condition->getPluginDefinition();
        $label = $plugin_definition['label'] ?? '';
        $list['#items'][] = $label . ': ' . $summary;
      }
    }

    if (empty($list['#items'])) {
      $list['#items'][] = $this->t('None');
    }

    return $list;
  }

  /**
   * Get condition summaries.
   *
   * @param \Drupal\Core\Condition\ConditionPluginCollection $conditions
   *   A collection of visibility conditions from the block config.
   *
   * @return array
   *   Condition summaries.
   */
  protected function getConditionSummaries(ConditionPluginCollection $conditions): array {
    $result = [];
    foreach ($conditions as $condition_id => $condition) {
      // Catch every exception that can be thrown by the condition plugin.
      // This way we can still display the other conditions.
      // And we prevent page from not being displayed.
      // A bad implementation in the summary method, for example using invalid
      // contexts, can cause a fatal error.
      try {
        $summary = (string) $condition->summary();
      }
      catch (\Exception $e) {
        $summary = $this->t('Unknown conditions');
      }
      finally {
        $plugin_definition = $condition->getPluginDefinition();
        $label = $plugin_definition['label'] ?? '';
        $result[$condition_id] = $label . ': ' . $summary;
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataOperationResult(string $operation = '') {
    $system_theme_config = $this->configFactory->get('system.theme');
    $default_theme = $system_theme_config->get('default');
    $admin_theme = $system_theme_config->get('admin');

    $table_header = [
      $this->t('Id'),
      $this->t('Label'),
      $this->t('Theme'),
      $this->t('Region'),
      $this->t('Status'),
      $this->t('Visibility Conditions'),
      $this->t('Link'),
    ];

    $block_storage = $this->entityTypeManager->getStorage('block');
    /** @var \Drupal\block\Entity\Block[] $blocks */
    $blocks = $block_storage->loadMultiple();
    $csv_data = [];

    $table_content = [];
    foreach ($blocks as $block) {
      $plane_visibility_conditions = $this->getConditionSummaries($block->getVisibilityConditions());
      $formatted_visibility_conditions = [
        '#theme' => 'item_list',
        '#items' => $plane_visibility_conditions,
        '#attributes' => ['class' => ['visibility-conditions-list']],
      ];
      $theme = $block->getTheme();

      if ($theme == $default_theme) {
        $theme_label = $theme . ' (default)';
      }
      elseif ($theme == $admin_theme) {
        $theme_label = $theme . ' (admin)';
      }
      else {
        $theme_label = $theme;
      }

      $table_content[$block->id()] = [
        'id' => $block->id(),
        'title' => $block->label(),
        'theme' => $theme_label,
        'region' => $block->getRegion(),
        'status' => $block->status() ? $this->t('Enabled') : $this->t('Disabled'),
        'visibility' => $this->renderer->render($formatted_visibility_conditions),
        'link' => Link::fromTextAndUrl($this->t('Block page'), $block->toUrl()),
      ];
      $csv_data[$block->id()] = $plane_visibility_conditions;
    }

    array_multisort(
      array_column($table_content, 'theme'),
      array_column($table_content, 'region'),
      $table_content
    );

    $this->handleCsv($operation, $table_content, $csv_data);

    return [
      'header_table' => $table_header,
      'results_table' => $table_content,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildDataRenderArray(array $data, string $operation = '') {
    $build = parent::buildDataRenderArray($data, $operation);
    $build['download_link'] = $this->createRenderableLink($operation);
    $build['download_link']['#weight'] = -10;
    return $build;
  }

  /**
   * Get headers.
   *
   * @return array
   *   Headers.
   */
  protected function getHeaders(): array {
    return [
      'id' => $this->t('Id'),
      'label' => $this->t('Label'),
      'theme' => $this->t('Theme'),
      'region' => $this->t('Region'),
      'status' => $this->t('Status'),
      'visibility_conditions' => $this->t('Visibility Conditions'),
      'link' => $this->t('Link'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareCsvHeaders(string $operation): array {
    $headers = $this->getHeaders();
    unset($headers['link']);
    return $headers;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareCsvData(string $operation, array ...$data): array {
    [$table_content, $csv_data] = $data;
    foreach ($table_content as $key => &$item) {
      unset($item['link']);
      $item['visibility'] = (isset($csv_data[$key])) ? implode('|', $csv_data[$key]) : '';
    }
    return $table_content;
  }

}
