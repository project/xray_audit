<?php

namespace Drupal\xray_audit\Plugin\xray_audit\tasks\ContentMetric;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\xray_audit\Form\SettingsForm;
use Drupal\xray_audit\Services\EntityUseInterface;
use Drupal\xray_audit\Services\PluginRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin implementation of queries_data_node.
 *
 * @XrayAuditTaskPlugin (
 *   id = "queries_data_nodes",
 *   label = @Translation("Node reports"),
 *   description = @Translation("Metrics about node entities."),
 *   group = "content_metric",
 *   sort = 1,
 *   local_task = 1,
 *   operations = {
 *      "number_node_type" = {
 *          "label" = "Grouped by type",
 *          "description" = "Number of Nodes grouped by type."
 *       },
 *       "number_node_type_lang" = {
 *          "label" = "Grouped by type and language",
 *          "description" = "Number of Nodes grouped by type and language."
 *       },
 *      "number_nodes_by_revisions" = {
 *          "label" = "Grouped by revisions",
 *          "description" = "Nodes with the highest number of revisions."
 *       }
 *    },
 *   dependencies = {"node"}
 * )
 */
final class XrayAuditQueryTaskNodePlugin extends XrayAuditQueryTaskPluginBase {

  const XRA_NODE_TEMPORARY_TABLE_NAME = 'xra_nodes_hierarchy_tmp';

  /**
   * Service "xray_audit.entity_use_node".
   *
   * @var \Drupal\xray_audit\Services\EntityUseInterface
   */
  protected $serviceEntityUsenode;

  /**
   * Service "request_stack".
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Service "state".
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Service "config.factory".
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Service "module_handler".
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Configuration settings for Xray Audit.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $xrayAuditConfig;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Database connection.
   * @param \Drupal\xray_audit\Services\EntityUseInterface $entity_use_node
   *   Service "xray_audit.entity_use_node".
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Service "request_stack".
   * @param \Drupal\xray_audit\Services\PluginRepositoryInterface $xray_audit_plugin_repository
   *   Service "xray_audit.plugin_repository".
   * @param \Drupal\Core\State\StateInterface $state
   *   Service "state".
   *
   * @SuppressWarnings(PHPMD.ExcessiveParameterList)
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
    LanguageManagerInterface $language_manager,
    EntityUseInterface $entity_use_node,
    RequestStack $request_stack,
    PluginRepositoryInterface $xray_audit_plugin_repository,
    StateInterface $state,
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $database, $language_manager, $xray_audit_plugin_repository, $module_handler);
    $this->serviceEntityUsenode = $entity_use_node;
    $this->requestStack = $request_stack;
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->xrayAuditConfig = $this->configFactory->get(SettingsForm::SETTINGS);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('language_manager'),
      $container->get('xray_audit.entity_use_node'),
      $container->get('request_stack'),
      $container->get('xray_audit.plugin_repository'),
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDataOperationResult(string $operation = '') {
    $cid = $this->getPluginId() . ':' . $operation;

    $data = $this->pluginRepository->getCachedData($cid);
    if (!empty($data) && is_array($data)) {
      return $data;
    }

    switch ($operation) {
      case 'number_node_type':
        $data = $this->nodeByTypes();
        break;

      case 'number_node_type_lang':
        $data = $this->nodeTypesPerLanguage();
        break;

      case 'number_nodes_by_revisions':
        $data = $this->nodesWithExcessiveRevisions();
        break;
    }

    $this->pluginRepository->setCacheTagsInv($cid, $data, ['node_list']);
    return $data;
  }

  /**
   * Get data for operation "node_by_types".
   *
   * @return array
   *   Render array.
   */
  protected function nodeByTypes() {

    $alias_count = 'count';
    $headerTable = [
      $this->t('ID'),
      $this->t('Label'),
      $this->t('Total'),
      $this->t('Published'),
      $this->t('Unpublished'),
    ];
    $resultTable = [];

    // Get label of content types.
    $label_content_types = [];
    $types = $this->entityTypeManager->getStorage("node_type")->loadMultiple();
    foreach ($types as $key => $type) {
      $label_content_types[$key] = $type->label();
    }

    $bundles_counted = [];

    $query = $this->entityTypeManager->getStorage('node')->getAggregateQuery();

    $publish_bundle = $query->accessCheck(FALSE)
      ->condition('status', '1')
      ->groupBy('type')
      ->aggregate('nid', 'count', NULL, $alias_count)
      ->execute();
    /** @var array<string> $bundle */
    foreach ($publish_bundle as $bundle) {
      $bundles_counted[$bundle['type']]['publish'] = $bundle['count'];
    }

    $query = $this->entityTypeManager->getStorage('node')->getAggregateQuery();
    $unpublish_bundle = $query->accessCheck(FALSE)
      ->condition('status', '1', '!=')
      ->groupBy('type')
      ->aggregate('nid', 'count', NULL, $alias_count)
      ->execute();
    /** @var array<string> $bundle */
    foreach ($unpublish_bundle as $bundle) {
      $bundles_counted[$bundle['type']]['unpublish'] = $bundle['count'];
    }

    $total = 0;
    $total_publish = 0;
    $total_no_publish = 0;
    foreach ($label_content_types as $bundle => $content_type_label) {
      /** @var int $publish */
      $publish = $bundles_counted[$bundle]['publish'] ?? 0;
      /** @var int $unpublish */
      $unpublish = $bundles_counted[$bundle]['unpublish'] ?? 0;
      $resultTable[$bundle] = [
        'id' => $bundle,
        'label' => $content_type_label,
        'total' => $publish + $unpublish,
        'publish' => $publish,
        'no_publish' => $unpublish,
      ];
      $total += $publish + $unpublish;
      $total_publish += $publish;
      $total_no_publish += $unpublish;
    }
    $resultTable['total']['id'] = $this->t('TOTAL');
    $resultTable['total']['label'] = '---';
    $resultTable['total']['total'] = $total;
    $resultTable['total']['publish'] = $total_publish;
    $resultTable['total']['no-publish'] = $total_no_publish;

    return [
      'header_table' => $headerTable,
      'results_table' => $resultTable,
    ];

  }

  /**
   * Get data for operation "node_by_types_language".
   *
   * @return array
   *   Render array.
   */
  protected function nodeTypesPerLanguage() {

    $alias = 'count';

    $headerTable = [
      $this->t('ID'),
      $this->t('Label'),
      $this->t('Langcode'),
      $this->t('Total'),
    ];
    $resultTable = [];

    // Get label of content types.
    $label = [];
    $types = $this->entityTypeManager->getStorage("node_type")->loadMultiple();
    foreach ($types as $key => $type) {
      $label[$key] = $type->label();
    }

    $query = $this->entityTypeManager->getStorage("node")->getAggregateQuery();
    $result = $query->accessCheck(FALSE)
      ->currentRevision()
      ->aggregate('nid', 'COUNT', NULL, $alias)
      ->groupBy('langcode')
      ->groupBy('type')
      ->sort('type')
      ->sort('langcode')
      ->execute();

    /** @var array<string> $row */
    foreach ($result as $row) {
      $resultTable[] = [
        'id' => $row['type'],
        'label' => $label[$row['type']],
        'langcode' => $row['langcode'],
        'total' => $row['count'] ?? 0,
      ];
    }

    return [
      'header_table' => $headerTable,
      'results_table' => $resultTable,
    ];

  }

  /**
   * Get nodes with too many revisions.
   *
   * @return array
   *   Render array.
   */
  protected function nodesWithExcessiveRevisions() {
    $alias_count = 'count';

    $headerTable = [
      $this->t('ID'),
      $this->t('Label'),
      $this->t('Revisions'),
    ];
    $resultTable = [];

    $query = $this->database->select('node_revision', 'nr');
    $query->addField('nr', 'nid');
    $query->addExpression('COUNT(nr.vid)', $alias_count);
    $query->join('node_field_data', 'nfd', 'nr.nid = nfd.nid');
    $query->addExpression('MAX(nfd.title)', 'label');
    $query->groupBy('nr.nid');
    $query->orderBy($alias_count, 'DESC');
    $query->range(0, 20);

    $result = $query->execute()->fetchAll();

    $max_revision_count = $this->xrayAuditConfig->get('revisions_thresholds.node');

    foreach ($result as $row) {
      $nid = $row->nid;
      $label = $row->label;
      $revisions = $row->{$alias_count};

      $resultTable[$nid] = [
        'id' => $nid,
        'label' => $label,
        'revisions' => [
          'data' => $revisions,
        ],
      ];

      if (!empty($max_revision_count) && $revisions > $max_revision_count) {
        $resultTable[$nid]['revisions']['class'] = 'xray-audit-color-error';
      }
    }
    if (empty($resultTable)) {
      $resultTable[] = [
        'id' => $this->t('No nodes found'),
        'label' => '-',
        'revisions' => 0,
      ];
    }

    return [
      'header_table' => $headerTable,
      'results_table' => $resultTable,
      'extended_data_render' => [
        '#attached' => [
          'library' => [
            'xray_audit/color',
          ],
        ],
      ],
    ];
  }

}
