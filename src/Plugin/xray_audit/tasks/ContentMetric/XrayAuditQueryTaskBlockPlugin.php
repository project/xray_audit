<?php

namespace Drupal\xray_audit\Plugin\xray_audit\tasks\ContentMetric;

use Drupal\Core\Entity\Query\QueryAggregateInterface;

/**
 * Plugin implementation of queries_data_node.
 *
 * @XrayAuditTaskPlugin (
 *   id = "queries_data_blocks",
 *   label = @Translation("Blocks reports"),
 *   description = @Translation("Metrics about block entities."),
 *   group = "content_metric",
 *   sort = 2,
 *   local_task = 1,
 *   operations = {
 *     "number_block_group_type" = {
 *          "label" = "Grouped by type",
 *          "description" = "Number of Block grouped by type."
 *       },
 *     "number_block_group_type_lang" = {
 *          "label" = "Grouped by type and language",
 *          "description" = "Number of Block grouped by type and language."
 *      },
 *    },
 *   dependencies = {"block_content"},
 * )
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
final class XrayAuditQueryTaskBlockPlugin extends XrayAuditQueryTaskPluginBase {

  /**
   * Get base query.
   *
   * @return \Drupal\Core\Entity\Query\QueryAggregateInterface
   *   Query.
   */
  protected function getBaseQuery(): QueryAggregateInterface {
    $alias = 'count';
    $query = $this->entityTypeManager->getStorage('block_content')->getAggregateQuery()
      ->accessCheck(FALSE)
      ->currentRevision()
      ->groupBy('type')
      ->aggregate('id', 'COUNT', NULL, $alias)
      ->sort('type');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataOperationResult(string $operation = '') {
    $cid = $this->getPluginId() . ':' . $operation;

    $data = $this->pluginRepository->getCachedData($cid);
    if (!empty($data) && is_array($data)) {
      return $data;
    }

    switch ($operation) {
      case 'number_block_group_type':
        $data = $this->blocksCountByType();
        break;

      case 'number_block_group_type_lang':
        $data = $this->blocksCountByTypeAndLanguage();
        break;
    }

    $this->pluginRepository->setCacheTagsInv($cid, $data, ['block_content_list']);
    return $data;
  }

  /**
   * Get total block counts grouped by type.
   *
   * @return array
   *   Render array.
   */
  public function blocksCountByType() {
    $table_header = [
      $this->t('Type'),
      $this->t('Label'),
      $this->t('Total'),
    ];
    $table_content = [];
    $result = $this->getBaseQuery()
      ->execute();

    $block_content_types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple();

    /** @var mixed[] $row */
    foreach ($result as $row) {
      if (empty($row['type'])) {
        continue;
      }

      $table_content[] = [$row['type'], $block_content_types[$row['type']]->label(), $row['count']];
    }
    return [
      'header_table' => $table_header,
      'results_table' => $table_content,
    ];
  }

  /**
   * Block content counts.
   *
   * @return array
   *   Render array.
   */
  public function blocksCountByTypeAndLanguage() {
    $table_header = [
      $this->t('Type'),
      $this->t('Label'),
      $this->t('Langcode'),
      $this->t('Total'),
    ];

    $table_content = [];
    $result = $this->getBaseQuery()
      ->groupBy('langcode')
      ->sort('langcode')
      ->execute();

    $block_content_types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple();

    /** @var mixed[] $row */
    foreach ($result as $row) {
      if (empty($row['type'])) {
        continue;
      }

      $table_content[] = [$row['type'], $block_content_types[$row['type']]->label(), $row['langcode'], $row['count']];
    }
    return [
      'header_table' => $table_header,
      'results_table' => $table_content,
    ];
  }

}
