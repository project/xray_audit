<?php

namespace Drupal\xray_audit\Services;

use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Service to download a csv with the data.
 */
class CsvDownloadManager implements CsvDownloadManagerInterface {

  use StringTranslationTrait;

  /**
   * Service "xray_audit.plugin_repository".
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Service "xray_audit.plugin_repository"
   *
   * @var \Drupal\xray_audit\Services\PluginRepositoryInterface
   */
  protected $pluginRepository;

  /**
   * Constructs the service.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Service "request_stack".
   * @param \Drupal\xray_audit\Services\PluginRepositoryInterface $plugin_repository
   *   Service "xray_audit.plugin_repository".
   */
  public function __construct(RequestStack $requestStack, PluginRepositoryInterface $plugin_repository) {
    $this->requestStack = $requestStack;
    $this->pluginRepository = $plugin_repository;
  }

  /**
   * {@inheritDoc}
   */
  public function downloadCsv(): bool {
    // Obtaining the current request.
    $request = $this->requestStack->getCurrentRequest();
    if ($request === NULL) {
      return FALSE;
    }

    // Obtaining the parameter download of the current query.
    $queryParam = $request->query->get('0');
    if (!empty($queryParam) && $queryParam == 'download') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function createCsv(array $csvData, array $headers, string $operation = ''): ?StreamedResponse {

    // phpcs:ignore
    // From: http://obtao.com/blog/2013/12/export-data-to-a-csv-file-with-symfony/
    $response = new StreamedResponse(function () use ($headers, $csvData) {
      $handle = fopen('php://output', 'r+');
      if ($handle === FALSE) {
        return NULL;
      }

      fputcsv($handle, $headers);

      foreach ($csvData as $row) {
        fputcsv($handle, $row);
      }

      fclose($handle);
    });

    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="' . $operation . '-' . date('Y-m-d') . '.csv"');

    return $response->send();
  }

  /**
   * {@inheritDoc}
   */
  public function createLink(string $operation): Link {
    $url = $this->pluginRepository->getTaskPageOperationFromIdOperation(
      $operation,
      ['download']
    );

    $url->setOption('attributes', [
      'class' => ['button', 'button--primary', 'button--small'],
    ]);

    return Link::fromTextAndUrl($this->t('Download'), $url);
  }

}
