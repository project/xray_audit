<?php

namespace Drupal\xray_audit\Utils;

use Drupal\Core\Render\RendererInterface;
use Drupal\Component\Utility\DeprecationHelper;

/**
 *
 */
final class XrayAuditDeprecationHelper {

  /**
   * Renders a renderable array in isolation.
   *
   * This method ensures that the provided renderable array is rendered using
   * the appropriate renderer service. If the DeprecationHelper class exists,
   * it will use a backwards-compatible call to render the array based on the
   * Drupal version. Otherwise, it will use the plain render method.
   *
   * @param array $renderable
   *   The renderable array to be rendered.
   * @param \Drupal\Core\Render\RendererInterface|null $renderer
   *   (optional) The renderer service. If not provided, the default renderer
   *   service will be used.
   *
   * @return string
   *   The rendered output.
   */
  public static function renderInIsolation(array $renderable, ?RendererInterface $renderer = NULL) {
    $renderer ??= \Drupal::service('renderer');
    // Remove when drupal < 10.3 is no longer supported.
    // @phpstan-ignore-next-line render plain is deprecated in drupal 10.3.
    $render_plain = fn() => $renderer->renderPlain($renderable);

    $output = self::backwardsCompatibleCall(
      '10.3',
      fn() => $renderer->renderInIsolation($renderable),
      $render_plain,
      $render_plain
    );

    return $output;
  }

  /**
   * Executes a callable function in a backwards-compatible manner.
   *
   * This method checks if the `DeprecationHelper` class exists. If it does, it uses
   * `DeprecationHelper::backwardsCompatibleCall` to execute the appropriate callable
   * based on the current Drupal version and the specified deprecated version.
   * If the `DeprecationHelper` class does not exist, it falls back to executing
   * the provided fallback callable.
   *
   * @param string $deprecatedVersion
   *   The version in which the functionality was deprecated.
   * @param callable $currentCallable
   *   The callable to execute if the current version is not deprecated.
   * @param callable $deprecatedCallable
   *   The callable to execute if the current version is deprecated.
   * @param callable $fallbackCallable
   *   The fallback callable to execute if `DeprecationHelper` class does not exist.
   *
   * @return mixed
   *   The result of the executed callable.
   */
  public static function backwardsCompatibleCall(string $deprecatedVersion, callable $currentCallable, callable $deprecatedCallable, callable $fallbackCallable) {
    $output = NULL;

    if (class_exists(DeprecationHelper::class)) {
      $output = DeprecationHelper::backwardsCompatibleCall(
        \Drupal::VERSION,
        $deprecatedVersion,
        $currentCallable,
        $deprecatedCallable
      );
    }
    else {
      $output = $fallbackCallable();
    }
    return $output;
  }

}
