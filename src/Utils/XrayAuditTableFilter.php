<?php

namespace Drupal\xray_audit\Utils;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides utility functions for table filters.
 */
class XrayAuditTableFilter {

  /**
   * Generates a renderable filter input for a table.
   *
   * @param string $table_id
   *   The ID of the table.
   * @param array $columns_indexes
   *   The indexes of the columns.
   * @param string $placeholder
   *   The placeholder text for the input field.
   *
   * @return array
   *   A renderable array for the filter input.
   */
  public static function generateRenderableFilterInput(
    string $table_id,
    array $columns_indexes = [],
    ?string $placeholder = NULL,
    array $columns_labels = [],
  ): array {

    if (empty($placeholder) && !empty($columns_labels)) {
      $placeholder = static::generateFilterInputPlaceholder($columns_indexes, $columns_labels);
    }

    $renderable_filter_input = [
      '#type' => 'textfield',
      '#placeholder' => $placeholder,
      '#attributes' => [
        'data-xa-filter-table-target' => $table_id,
        'data-xa-filter-table-columns-indexes' => json_encode($columns_indexes),
      ],
      '#attached' => [
        'library' => [
          'xray_audit/table_filter',
        ],
      ],
    ];

    return $renderable_filter_input;
  }

  /**
   * Generates a placeholder text based on column indexes and columns_labels.
   *
   * @param array $columns_indexes
   *   The indexes of the columns.
   * @param array $columns_labels
   *   List of columns labels to use to generate part of the placeholder.
   *
   * @return string
   *   The generated placeholder text.
   */
  public static function generateFilterInputPlaceholder(array $columns_indexes, array $columns_labels): string {
    $placeholder_columns = [];

    foreach ($columns_indexes as $index) {
      if (isset($columns_labels[$index])) {
        $placeholder_columns[] = mb_strtolower((string) $columns_labels[$index]);
      }
    }

    $last_column = end($placeholder_columns);
    array_pop($placeholder_columns);

    return new TranslatableMarkup('Filter by @columns or @last_column', [
      '@columns' => implode(', ', $placeholder_columns),
      '@last_column' => $last_column,
    ]);
  }

}
