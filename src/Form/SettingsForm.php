<?php

declare(strict_types=1);

namespace Drupal\xray_audit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Xray Audit settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  const SETTINGS = 'xray_audit.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return static::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [static::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['revisions_thresholds'] = [
      '#type' => 'details',
      '#title' => $this->t('Revision Thresholds'),
      '#open' => TRUE,
      '#description' => $this->t('Specify the maximum number of revisions that will be considered excessive.'),
      '#tree' => TRUE,
    ];

    $form['revisions_thresholds']['node'] = [
      '#type' => 'number',
      '#title' => $this->t('Node threshold'),
      '#default_value' => $config->get('revisions_thresholds.node'),
      '#required' => TRUE,
    ];

    $form['revisions_thresholds']['paragraph'] = [
      '#type' => 'number',
      '#title' => $this->t('Paragraph threshold'),
      '#default_value' => $config->get('revisions_thresholds.paragraph'),
      '#required' => TRUE,
    ];

    $form['size_thresholds'] = [
      '#type' => 'details',
      '#title' => $this->t('Size Thresholds'),
      '#open' => TRUE,
      '#description' => $this->t('Specify the maximum size of tables that will be considered excessive.'),
      '#tree' => TRUE,
    ];

    $form['size_thresholds']['tables'] = [
      '#type' => 'number',
      '#title' => $this->t('Tables threshold (MB)'),
      '#default_value' => $config->get('size_thresholds.tables'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config(static::SETTINGS);

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
