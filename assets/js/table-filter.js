/**
 * @file
 * Xray Audit Table Filter.
 */
((Drupal, once) => {
  'use strict';

  Drupal.behaviors.xrayAuditTableFilter = {
    attach: (context, settings) => {
      once('xrayAuditTableFilter', 'input[data-xa-filter-table-target]', context).forEach((input) => {
        input.addEventListener('input', (event) => {
          const target_selector = input.getAttribute('data-xa-filter-table-target');
          const table = document.getElementById(target_selector);
          const filter_text = event.target.value.toLowerCase();
          const columns_to_filter = JSON.parse(input.getAttribute('data-xa-filter-table-columns-indexes'));

          if (Boolean(table)) {
            const rows = table.querySelectorAll('tbody tr');
            rows.forEach((row) => {
                const cells = row.querySelectorAll('td');
                let row_text;

                if (Boolean(columns_to_filter) && columns_to_filter.length > 0) {
                  row_text = columns_to_filter.map(index => cells[index]?.textContent.toLowerCase() || '').join(' ');
                } else {
                  row_text = Array.from(cells).map(cell => cell.textContent.toLowerCase()).join(' ');
                }
                row.style.display = row_text.includes(filter_text) ? '' : 'none';
            });
          }
        })
      });
    }
  };

})(Drupal, once);
